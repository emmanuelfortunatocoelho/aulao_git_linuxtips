import glob
from pathlib import Path
import datetime
import time
import subprocess
import mysql.connector
import hashlib

db = mysql.connector.connect(
  host="192.168.5.199",
  user="root",
  password="novasenhalocal123",
  database="converter"
  )

def create(args):
    if args.path.endswith('/'):
        args.path = args.path[:-1]
    path = args.path
    path_full = args.path + '/*/*/*'
    if Path(path).is_dir():
        print("-" * 88)
        print("Starting to create the videos list to be converted...")
        for name in glob.glob(path_full):
            if name.lower().endswith(('.mp4', '.mov', '.avi')):
                FFMPEG_TEST_ORIGINAL_COMMAND = "ffmpeg -v error -i %s -f null - 2>>/dev/null" % name
                testing = subprocess.call(FFMPEG_TEST_ORIGINAL_COMMAND, shell=True)
                if testing == 0:            
                    file_to_test = open(name, "rb")
                    data = file_to_test.read()
                    md5_returned = hashlib.md5(data).hexdigest()
                    check_if_name_exist = "SELECT name FROM list WHERE MD5 = '%s'" % md5_returned
                    check_cursor = db.cursor(buffered=True)
                    check_cursor.execute(check_if_name_exist)
                    check_if_name_exist_result = check_cursor.rowcount
                    if check_if_name_exist_result == 0:
                        insert_cursor = db.cursor(buffered=True)
                        sql = "INSERT INTO list (date, name, MD5, converted, failed, name_converted ) VALUES ('%s', '%s', '%s', False, False, '')" % (datetime.datetime.now(), name, md5_returned)
                        insert_cursor.execute(sql)
                        db.commit()
                        print("%s -[ADDED] - %s" % (datetime.datetime.now(), name))

                    else:
                        print("%s - [WARNING] - %s - Video already exist in the database" % (datetime.datetime.now(), name))
            else:
                print("%s - [ERROR] - %s - This format is not supported." % (datetime.datetime.now(), name)) 
    else:
        print("ERROR - %s - Directory don't exist" % path)
        exit(1)
    return
